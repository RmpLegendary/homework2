package task5.service;

import task5.dao.AccountRepository;
import task5.dao.UserAccountNotFoundException;
import task5.model.UserAccount;

import java.io.IOException;
import java.util.UUID;

public class UserAccountService {
    private final AccountRepository accountRepository;

    public UserAccountService(AccountRepository accountRepository) {this.accountRepository = accountRepository;}

    public UserAccount createAccount(String userName) throws IOException {
        synchronized (userName) {
            UserAccount userAccount = new UserAccount(userName, UUID.randomUUID().toString());
            accountRepository.save(userAccount);
            return userAccount;
        }
    }

    public UserAccount getAccount(String userName, String accountNumber) throws UserAccountNotFoundException {
        synchronized (userName) {
            return accountRepository.get(userName, accountNumber);
        }
    }

    public void save(UserAccount userAccount) throws IOException {
        synchronized (userAccount.getUserName()) {
            accountRepository.save(userAccount);
        }
    }
}
