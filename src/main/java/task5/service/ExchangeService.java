package task5.service;

import task5.dao.ExchangeRateNotFoundException;
import task5.dao.ExchangeRateRepository;
import task5.model.Currency;
import task5.model.ExchangeRate;
import task5.model.UserAccount;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class ExchangeService {
    private final ExchangeRateRepository exchangeRateRepository;

    public ExchangeService(ExchangeRateRepository exchangeRateRepository) {this.exchangeRateRepository = exchangeRateRepository;}

    public void exchange(UserAccount account, BigDecimal exchangeAmount, Currency fromCurrency, Currency toCurrency)
            throws ExchangeRateNotFoundException, InsufficientAmountException {
        synchronized (account.getAccountNumber()) {
            BigDecimal amountFrom = account.getAmount(fromCurrency);
            if (amountFrom.compareTo(exchangeAmount) < 0) {
                throw new InsufficientAmountException();
            }
            ExchangeRate exchangeRate = exchangeRateRepository.get(fromCurrency, toCurrency);

            BigDecimal exchangeResult = exchangeAmount.multiply(exchangeRate.getExchangeRate());
            account.addAmount(toCurrency, exchangeResult);
            account.addAmount(fromCurrency, exchangeAmount.negate());
        }
    }

    public void save(ExchangeRate exchangeRate) {
        exchangeRateRepository.save(exchangeRate);
        ExchangeRate reverseExchange = new ExchangeRate(
                exchangeRate.getCurrency2(),
                exchangeRate.getCurrency1(),
                BigDecimal.ONE.divide(exchangeRate.getExchangeRate(), 5, RoundingMode.HALF_DOWN)
        );
        exchangeRateRepository.save(reverseExchange);
    }
}
