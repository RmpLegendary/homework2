package task5;

import task5.dao.AccountRepository;
import task5.dao.ExchangeRateNotFoundException;
import task5.dao.ExchangeRateRepository;
import task5.model.Currency;
import task5.model.ExchangeRate;
import task5.model.UserAccount;
import task5.service.ExchangeService;
import task5.service.InsufficientAmountException;
import task5.service.UserAccountService;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {

    private static final UserAccountService USER_ACCOUNT_SERVICE = getUserAccountService();
    private static final ExchangeService EXCHANGE_SERVICE = getExchangeService();
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(10);

    public static void main(String[] args) throws IOException {

        UserAccount userAccount1 = USER_ACCOUNT_SERVICE.createAccount("test1");
        UserAccount userAccount2 = USER_ACCOUNT_SERVICE.createAccount("test2");

        userAccount1.addAmount(Currency.USD, new BigDecimal(10000));
        userAccount1.addAmount(Currency.CAD, new BigDecimal(10000));
        userAccount1.addAmount(Currency.EUR, new BigDecimal(10000));
        userAccount2.addAmount(Currency.USD, new BigDecimal(10000));
        userAccount2.addAmount(Currency.CAD, new BigDecimal(10000));
        userAccount2.addAmount(Currency.EUR, new BigDecimal(10000));

        USER_ACCOUNT_SERVICE.save(userAccount1);
        USER_ACCOUNT_SERVICE.save(userAccount2);

        EXCHANGE_SERVICE.save(new ExchangeRate(Currency.USD, Currency.EUR, BigDecimal.valueOf(95, 2)));
        EXCHANGE_SERVICE.save(new ExchangeRate(Currency.USD, Currency.CAD, BigDecimal.valueOf(130, 2)));
        EXCHANGE_SERVICE.save(new ExchangeRate(Currency.EUR, Currency.CAD, BigDecimal.valueOf(137, 2)));


        EXECUTOR_SERVICE.submit(() -> exchangeRandomly(userAccount1, Currency.EUR, Currency.USD));
        EXECUTOR_SERVICE.submit(() -> exchangeRandomly(userAccount1, Currency.USD, Currency.CAD));
        EXECUTOR_SERVICE.submit(() -> exchangeRandomly(userAccount1, Currency.CAD, Currency.EUR));
        EXECUTOR_SERVICE.submit(() -> exchangeRandomly(userAccount2, Currency.EUR, Currency.USD));
        EXECUTOR_SERVICE.submit(() -> exchangeRandomly(userAccount2, Currency.USD, Currency.CAD));
        EXECUTOR_SERVICE.submit(() -> exchangeRandomly(userAccount2, Currency.CAD, Currency.EUR));
    }

    private static ExchangeService getExchangeService() {
        return new ExchangeService(new ExchangeRateRepository());
    }

    private static UserAccountService getUserAccountService() {
        return new UserAccountService(new AccountRepository());
    }

    private static void exchangeRandomly(UserAccount userAccount1, Currency fromCurrency, Currency toCurrency) {
        Random random = new Random();
        while (true) {
            try {
                EXCHANGE_SERVICE.exchange(userAccount1, BigDecimal.valueOf(random.nextInt(100)), fromCurrency, toCurrency);
                System.out.println(userAccount1);
            } catch (ExchangeRateNotFoundException | InsufficientAmountException e) {
                System.out.println(e);
            }
        }
    }
}
