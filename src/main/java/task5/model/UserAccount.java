package task5.model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class UserAccount {
    private final String userName;
    private final String accountNumber;

    private final Map<Currency,BigDecimal> amounts = new HashMap<>();

    public UserAccount(String userName, String accountNumber) {
        this.userName = userName;
        this.accountNumber = accountNumber;
    }

    public BigDecimal getAmount(Currency currency) {
        return amounts.get(currency);
    }

    public synchronized void addAmount(Currency currency, BigDecimal amount) {
        BigDecimal currentAmount = amounts.computeIfAbsent(currency, c -> new BigDecimal(0));
        amounts.put(currency, currentAmount.add(amount));
    }

    public String getUserName() {
        return userName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public Map<Currency, BigDecimal> getAmounts() {
        return new HashMap<>(amounts);
    }

    @Override
    public String toString() {
        return "UserAccount{" +
               "userName='" + userName + '\'' +
               ", accountNumber='" + accountNumber + '\'' +
               ", amounts=" + amounts +
               '}';
    }
}
