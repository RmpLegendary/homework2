package task5.model;

import java.math.BigDecimal;
import java.util.Objects;

public class ExchangeRate {
    private final Currency currency1;
    private final Currency currency2;

    private BigDecimal exchangeRate;

    public ExchangeRate(
            Currency currency1,
            Currency currency2,
            BigDecimal sellRate
    ) {
        this.currency1 = currency1;
        this.currency2 = currency2;
        this.exchangeRate = sellRate;
    }

    public Currency getCurrency1() {
        return currency1;
    }

    public Currency getCurrency2() {
        return currency2;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ExchangeRate that = (ExchangeRate) o;
        return currency1 == that.currency1 && currency2 == that.currency2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(currency1, currency2);
    }
}
