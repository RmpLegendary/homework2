package task5.dao;

import task5.model.Currency;
import task5.model.ExchangeRate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import static java.math.BigDecimal.ZERO;

public class ExchangeRateRepository {

    private final Map<ExchangeRate, ExchangeRate> repository = new ConcurrentHashMap<>();

    public void save(ExchangeRate exchangeRate){
        repository.put(exchangeRate,exchangeRate);
    }

    public ExchangeRate get(Currency currency1, Currency currency2) throws ExchangeRateNotFoundException {
        ExchangeRate exchangeRate = repository.get(new ExchangeRate(currency1, currency2, ZERO));
        if(exchangeRate == null){
            throw new ExchangeRateNotFoundException();
        }
        return exchangeRate;
    }

}
