package task5.dao;

import task5.model.Currency;
import task5.model.UserAccount;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Scanner;

public class AccountRepository {
    public void save(UserAccount userAccount) throws IOException {
        File userDirectory = getDir(userAccount.getUserName());
        File accountFile = getFile(userDirectory + "/" + userAccount.getAccountNumber());
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(accountFile);
            Map<Currency, BigDecimal> amounts = userAccount.getAmounts();
            for (Map.Entry<Currency, BigDecimal> entry : amounts.entrySet()) {
                Currency c = entry.getKey();
                BigDecimal a = entry.getValue();
                fileWriter.write(c.name() + "\t" + a.toString() + "\n");
            }
        } catch (IOException e) {
            if (fileWriter != null) {
                try {
                    fileWriter.flush();
                    fileWriter.close();
                } catch (IOException ignore) {}
            }
        }
    }

    public UserAccount get(String userName, String accountNumber) throws UserAccountNotFoundException {
        File accountFile = new File(userName + "/" + accountNumber);
        if (!accountFile.exists()) {
            throw new UserAccountNotFoundException("account file does not exist");
        }

        UserAccount userAccount = new UserAccount(userName, accountNumber);

        try {
            Scanner scanner = new Scanner(accountFile);
            String amount;
            while ((amount = scanner.nextLine()) != null) {
                String[] currencyAndValue = amount.split("\t");
                userAccount.addAmount(Currency.valueOf(currencyAndValue[0]), new BigDecimal(currencyAndValue[1]));
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return userAccount;
    }

    private File getDir(String path) throws IOException {
        File accountFile = new File(path);
        if (!accountFile.exists()) {
            if (!accountFile.mkdir()) {
                throw new IOException("Can't create directory to store user accounts data");
            }
        }
        return accountFile;
    }

    private File getFile(String path) {
        File accountFile = new File(path);
        if (!accountFile.exists()) {
            try {
                if (!accountFile.createNewFile()) {
                    throw new RuntimeException("Can't create file to store user accounts data");
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return accountFile;
    }
}
