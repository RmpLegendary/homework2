package task5.dao;

public class UserAccountNotFoundException extends Exception {
    public UserAccountNotFoundException(String errorText) {
        super(errorText);
    }
}
