package task3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {
    public static void main(String[] args) {
        List<String> messageList = new ArrayList<>();
        Map<String, List<String>> customQueue = new HashMap<>();
        String topic = "CustomTopic";
        String message = "Message with random number ";
        ExecutorService executor = Executors.newFixedThreadPool(5);
        while (true) {
            //sending messages
            executor.execute(new Producer(customQueue, topic, message + (int) (Math.random() * 100), messageList));

            //consuming messages
            executor.execute(new Consumer(customQueue, topic));
        }
    }
}
