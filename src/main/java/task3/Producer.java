package task3;

import java.util.List;
import java.util.Map;

public class Producer implements Runnable {
    Map<String, List<String>> customQueue;
    String topic;
    String message;
    List<String> messageList;

    public Producer(Map<String, List<String>> customQueue, String topic, String message, List<String> messageList) {
        this.customQueue = customQueue;
        this.topic = topic;
        this.message = message;
        this.messageList = messageList;
    }

    public void sendMessage(String topic, String message) {
        messageList.add(message);
        customQueue.put(topic, messageList);
    }

    @Override
    public void run() {
        System.out.println("Thread name: " + Thread.currentThread().getName() + " is sending message with topic = " + topic + " and Message = " + message);
        sendMessage(topic, message);
    }
}
