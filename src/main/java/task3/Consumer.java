package task3;

import java.util.List;
import java.util.Map;

public class Consumer implements Runnable {
    Map<String, List<String>> customQueue;
    String topic;

    public Consumer(Map<String, List<String>> customQueue, String topic) {
        this.customQueue = customQueue;
        this.topic = topic;
    }

    public String readMessage() {
        String message = customQueue.get(topic).get(0);
        customQueue.get(topic).remove(0);
        return message;
    }

    @Override
    public void run() {
        if (customQueue.get(topic).size() > 0) {
            System.out.println("Thread name: " + Thread.currentThread().getName() + " is consuming message: " + readMessage() + "from topic: " + topic);
        }
    }
}
