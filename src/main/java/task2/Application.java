package task2;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {
    public static void main(String[] args) {
        Map<Integer, Integer> chmap = new HashMap<>();
        ExecutorService executor = Executors.newFixedThreadPool(3);
        CountDownLatch latch = new CountDownLatch(3);

        while (true) {
            //filling map with random integers infinitive
            executor.execute(new Filler(chmap, latch));

            //calculating map values every 2 sec
            executor.execute(new Calculator(chmap, latch));

            //calculating sqrt of map values sum every 2 sec
            executor.execute(new Sqr(chmap, latch));
        }
    }
}
