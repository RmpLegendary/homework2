package task2;

import java.util.Map;
import java.util.concurrent.CountDownLatch;

import static java.lang.Math.floor;
import static java.lang.Math.round;

public class Sqr implements Runnable {
    final Map<Integer, Integer> map;
    CountDownLatch countDownLatch;

    public Sqr(Map<Integer, Integer> map, CountDownLatch countDownLatch) {
        this.map = map;
        this.countDownLatch = countDownLatch;
    }

    public void calculateSqr() {
        int result = 0;
        for (Integer value : map.values()) {
            int sqr = value * value;
            result += sqr;
        }
        System.out.println("Sqrt of Map elements sum = " + Math.sqrt(result));
    }

    @Override
    public void run() {
        synchronized (map){
            calculateSqr();
            countDownLatch.countDown();
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
