package task2;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class Filler implements Runnable {
    final Map<Integer, Integer> map;
    CountDownLatch countDownLatch;

    public Filler(Map<Integer, Integer> map, CountDownLatch countDownLatch) {
        this.map = map;
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        Random random = new Random();
        synchronized (map) {
            int randomValue = random.nextInt(10);
            map.put(random.nextInt(), randomValue);
            System.out.println("Inserted value = " + randomValue);
            countDownLatch.countDown();
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}