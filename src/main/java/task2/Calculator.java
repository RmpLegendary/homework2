package task2;

import java.util.Map;
import java.util.concurrent.CountDownLatch;


public class Calculator implements Runnable {
    final Map<Integer, Integer> map;
    CountDownLatch countDownLatch;

    public Calculator(Map<Integer, Integer> map, CountDownLatch countDownLatch) {
        this.map = map;
        this.countDownLatch = countDownLatch;
    }

    public void calculate() {
        int result = 0;
        for (Integer value : map.values()) {
            result += value;
        }
        System.out.println("Map values sum = " + result);
    }

    @Override
    public void run() {
        synchronized (map) {
            calculate();
            countDownLatch.countDown();
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
