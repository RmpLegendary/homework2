package task1;

import java.util.Map;
import java.util.concurrent.locks.Lock;

public class CalculatorWithLock {
    final Map<Integer, Integer> map;
    Lock lock;

    public CalculatorWithLock(Map<Integer, Integer> map, Lock lock) {
        this.map = map;
        this.lock = lock;
    }

    public void calculate() throws InterruptedException {
        int result = 0;
        if (lock.tryLock()) {
            try {
                lock.lock();
                for (Integer value : map.keySet()) {
                    result += value;
                }
                System.out.println("Map values sum = " + result);
            } finally {
                lock.unlock();
            }
        } else {
            Thread.sleep(1000);
            calculate();
        }
    }
}
