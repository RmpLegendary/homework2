package task1;

import java.util.Map;
import java.util.concurrent.locks.Lock;

public class FillerWithLock implements Runnable {
    Map<Integer, Integer> map;
    Lock lock;
    private static final int MAX_SIZE = 500000;

    public FillerWithLock(Map<Integer, Integer> map, Lock lock) {
        this.map = map;
        this.lock = lock;
    }

    @Override
    public void run() {
        try {
            lock.lock();
            for (int i = 0; i < MAX_SIZE; i++) {
                map.put(i, i);
            }
        } finally {
            lock.unlock();
        }
    }
}