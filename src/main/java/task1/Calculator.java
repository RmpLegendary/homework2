package task1;

import java.util.Map;

public class Calculator {
    final Map<Integer, Integer> map;

    public Calculator(Map<Integer, Integer> map) {
        this.map = map;
    }

    public void calculate() {
        int result = 0;
        synchronized (map) {
            for (Integer value : map.values()) {
                result += value;
            }
        }
        System.out.println("Map values sum = " + result);
    }
}
