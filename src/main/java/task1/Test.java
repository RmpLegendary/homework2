package task1;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Test {

    public void startExample() {
        try {
            //hashMapExample();
            hashMapWithLockExample();
            //syncMapExample();
            //concurrentHashMapExample();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void syncMapExample() throws InterruptedException {
        Map<Integer, Integer> syncMap = Collections.synchronizedMap(new HashMap<>());
        calculate(syncMap);
    }

    public void hashMapExample() throws InterruptedException {
        Map<Integer, Integer> hmap = new HashMap<>();
        calculate(hmap);
    }

    public void hashMapWithLockExample() throws InterruptedException {
        Map<Integer, Integer> hmap = new HashMap<>();
        calculateWithLock(hmap);
    }

    public void concurrentHashMapExample() throws InterruptedException {
        Map<Integer, Integer> chmap = new ConcurrentHashMap<>();
        calculate(chmap);
    }

    private void calculate(Map<Integer, Integer> map) throws InterruptedException {
        Thread fillerThread = new Thread(new Filler(map));
        fillerThread.start();
        new Calculator(map).calculate();
        fillerThread.join();
    }

    private void calculateWithLock(Map<Integer, Integer> map) throws InterruptedException {
        Lock reLock = new ReentrantLock();
        Thread fillerThread = new Thread(new FillerWithLock(map, reLock));
        fillerThread.start();
        new CalculatorWithLock(map, reLock).calculate();
        fillerThread.join();
    }
}
