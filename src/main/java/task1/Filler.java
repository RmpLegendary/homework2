package task1;

import java.util.Map;

public class Filler implements Runnable {
    Map<Integer, Integer> map;
    private static final int MAX_SIZE = 500000;

    public Filler(Map<Integer, Integer> map) {
        this.map = map;
    }

    @Override
    public void run() {
        for (int i = 0; i < MAX_SIZE; i++) {
            map.put(i, i);
        }
    }
}