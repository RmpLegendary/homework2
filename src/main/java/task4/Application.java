package task4;

public class Application {
    public static void main(String[] args) {
        BlockingObjectPool pool = new BlockingObjectPool(5);

        //inserting into pool
        Thread insertThread = new Thread(() -> {
            while (true) {
                pool.put(new Object());
            }
        });
        insertThread.start();

        //checking size of the pool
        Thread sizeChecker = new Thread(() -> {
            while (true) {
                System.out.println("Pool size: " + pool.getSize());
            }
        });
        sizeChecker.start();

        //removing from the pool
        Thread removeThread = new Thread(() -> {
            while (true) {
                System.out.println("Removing: " + pool.get());
            }
        });

        removeThread.start();

        try {
            insertThread.join();
            sizeChecker.join();
            removeThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
