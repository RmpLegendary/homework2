package task4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class BlockingObjectPool {
    private final List<Object> objectPool = new ArrayList<>();
    private final int poolLimit;
    ReentrantLock reentrantLock = new ReentrantLock();
    Condition emptyCondition = reentrantLock.newCondition();
    Condition fullCondition = reentrantLock.newCondition();

    public BlockingObjectPool(int poolLimit) {
        this.poolLimit = poolLimit;
    }

    public Object get() {
        reentrantLock.lock();
        Object objectToRemove = null;
        try {
            while (objectPool.size() == 0) {
                emptyCondition.await();
            }
            objectToRemove = objectPool.get(0);
            objectPool.remove(0);
            fullCondition.signal();
            return objectToRemove;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            reentrantLock.unlock();
        }
        return objectToRemove;
    }

    public void put(Object object) {
        reentrantLock.lock();
        try {
            while (objectPool.size() >= poolLimit) {
                fullCondition.await();
            }
            objectPool.add(object);
            emptyCondition.signal();
            System.out.println("Inserting " + object);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            reentrantLock.unlock();
        }
    }

    public int getSize() {
        return objectPool.size();
    }
}
